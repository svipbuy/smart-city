package ${EntityPackageName};

import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
 @ApiModel(description = "导入对象-${ClassName}Import")
 @NoArgsConstructor
 @AllArgsConstructor
 @ToString
 @Data
public class ${ClassName}Import extends ${ClassName} {
    // 行号，必须加
    private Integer row;

    // 是否成功，必须加
    private Boolean successFlag;

    // 错误原因，必须加
    private String errorMessage;
}