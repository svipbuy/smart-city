package com.sc.generatorcode.entity;

import java.io.Serializable;

/**
 * @author ：wust
 * Date   2018/9/7
 */
public class Configuration implements Serializable {
    private String author;
    private Path path;
    private Db db;
    private String serverName;
    private String moduleName;
    private String axiosReqPrefix;
    private BasePackage basePackage;

    public Configuration() {
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public Db getDb() {
        return db;
    }

    public void setDb(Db db) {
        this.db = db;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getAxiosReqPrefix() {
        return axiosReqPrefix;
    }

    public void setAxiosReqPrefix(String axiosReqPrefix) {
        this.axiosReqPrefix = axiosReqPrefix;
    }

    public BasePackage getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(BasePackage basePackage) {
        this.basePackage = basePackage;
    }

    public static class Db {
        private String url;
        private String username;
        private String password;
        private String driver;

        public Db() {
        }

        public Db(String url, String username, String password, String driver) {
            this.url = url;
            this.username = username;
            this.password = password;
            this.driver = driver;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }
    }

    public static class BasePackage{
        private String base;
        private String corePackageName;
        private String controller;
        private String service;
        private String interf;
        private String dao;
        private String entityPackageName;

        public BasePackage() {
        }

        public BasePackage(String base, String corePackageName, String controller, String service, String interf, String dao, String entityPackageName) {
            this.base = base;
            this.corePackageName = corePackageName;
            this.controller = controller;
            this.service = service;
            this.interf = interf;
            this.dao = dao;
            this.entityPackageName = entityPackageName;
        }


        public String getBase() {
            return base;
        }

        public void setBase(String base) {
            this.base = base;
        }

        public String getCorePackageName() {
            return corePackageName;
        }

        public void setCorePackageName(String corePackageName) {
            this.corePackageName = corePackageName;
        }

        public String getController() {
            return controller;
        }

        public void setController(String controller) {
            this.controller = controller;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getInterf() {
            return interf;
        }

        public void setInterf(String interf) {
            this.interf = interf;
        }

        public String getDao() {
            return dao;
        }

        public void setDao(String dao) {
            this.dao = dao;
        }

        public String getEntityPackageName() {
            return entityPackageName;
        }

        public void setEntityPackageName(String entityPackageName) {
            this.entityPackageName = entityPackageName;
        }
    }

    public static class Path {
        private String mapper;
        private String entityDir;
        private String vueDir;


        public Path() {
        }


        public Path(String mapper, String entityDir, String vueDir) {
            this.mapper = mapper;
            this.entityDir = entityDir;
            this.vueDir = vueDir;
        }

        public String getEntityDir() {
            return entityDir;
        }

        public void setEntityDir(String entityDir) {
            this.entityDir = entityDir;
        }

        public String getMapper() {
            return mapper;
        }

        public void setMapper(String mapper) {
            this.mapper = mapper;
        }

        public String getVueDir() {
            return vueDir;
        }

        public void setVueDir(String vueDir) {
            this.vueDir = vueDir;
        }
    }

}
