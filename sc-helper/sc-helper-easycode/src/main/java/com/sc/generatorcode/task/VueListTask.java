package com.sc.generatorcode.task;

import com.sc.generatorcode.entity.ColumnInfo;
import com.sc.generatorcode.task.base.AbstractTask;
import com.sc.generatorcode.utils.ConfigUtil;
import com.sc.generatorcode.utils.FileUtil;
import com.sc.generatorcode.utils.FreemarkerConfigUtils;
import com.sc.generatorcode.utils.StringUtil;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：wust
 * @date：2019-12-26
 */
public class VueListTask extends AbstractTask {
    public VueListTask(String className, List<ColumnInfo> infos) {
        super(className,infos);
    }

    @Override
    public void run() throws IOException, TemplateException {
        String postfixName =  super.getName()[0];
        String name = super.getName()[1];


        StringBuffer tableColumnStr = new StringBuffer();
        if(columnInfos != null && columnInfos.size() > 0){
            int i = 0;
            for (ColumnInfo columnInfo : columnInfos) {
                if("id".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createrName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("createTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyName".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("modifyTime".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("projectId".equals(columnInfo.getPropertyName())){
                    continue;
                }
                if("companyId".equals(columnInfo.getPropertyName())){
                    continue;
                }

                tableColumnStr.append("<el-table-column label=\"" + columnInfo.getColumnLabel() + "\" prop=\"" + columnInfo.getPropertyName() + "\" width=\"120\">");
                tableColumnStr.append("\n</el-table-column>\n");
            }
        }

        Map<String, String> data = new HashMap<>();
        data.put("Author", ConfigUtil.getConfiguration().getAuthor());
        data.put("DateTime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
        data.put("moduleName", postfixName);
        data.put("name", name);
        data.put("tableColumn",tableColumnStr.toString());
        data.put("pageName", postfixName + "List");
        data.put("axiosReqPrefix", ConfigUtil.getConfiguration().getAxiosReqPrefix());
        data.put("xmlName",  ConfigUtil.getConfiguration().getServerName().concat("-").concat(name).replaceAll("-","_"));
        String filePath = StringUtil.package2Path(ConfigUtil.getConfiguration().getPath().getVueDir());

        String fileName = name + "-list.vue";
        FileUtil.generateFile(FreemarkerConfigUtils.TYPE_VUE_LIST, data, filePath + File.separator + name,fileName);
    }
}
