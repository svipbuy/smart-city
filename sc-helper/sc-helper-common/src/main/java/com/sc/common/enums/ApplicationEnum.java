package com.sc.common.enums;


/**
 * 应用枚举
 * Created by wust on 2019/3/29.
 */
public enum  ApplicationEnum {

    RC4_TOKEN_KEY("16d7c967-6177-11e9-a68d-0050568e63cd"), // token的RC4加密密钥

    WEB_LOGIN_KEY("WEB_LOGIN_KEY=%s=%s"), // WEB_LOGIN_KEY=loginName=token

    APP_LOGIN_KEY("APP_LOGIN_KEY=%s=%s"),// APP_LOGIN_KEY=loginName=token

    X_AUTH_TOKEN("x-auth-token"), // 前后端交互认证token，前端和后端交互的token（前端包括web和手机）

    X_CTX("x-ctx"), // 上下文传递key

    X_LOCALE("x-locale"), // 前后端交互的语言环境

    X_AUTH_TOKEN_EXPIRE_TIME(10), // web前端与后端交互认证token失效时间，单位：分钟

    X_APP_AUTH_TOKEN_EXPIRE_TIME(30 * 24 * 60), // 手机app与后端交互认证token失效时间，单位：分钟

    SYSTEM_ADMIN_ACCOUNT("admin"), // 系统超级管理员账号

    LOGIN_RC4_KEY("jZ5$x!6yeAo1Qe^r89"), // 用户账号密码RC4秘钥

    QRCode_LOGIN_RC4_KEY("jZ5$x!6yeAo1Qe^r_@"), // 扫码登录的RC4加密密钥

    MENU_TYPE_M("m"),              // 资源类型，菜单

    MENU_TYPE_R("r"),              // 资源类型，按钮

    DEFAULT,

    DEFAULT_AGENT_NAME("研发公司自营代理"),

    DEFAULT_PARENT_COMPANY_NAME("研发公司自营总公司"),

    DEFAULT_BRANCH_COMPANY_NAME("研发公司自营分公司"),

    DEFAULT_PROJECT_NAME("内部测试项目");

    ApplicationEnum(){}

    ApplicationEnum(String stringValue){
        this.stringValue = stringValue;
    }

    ApplicationEnum(int intValue){
        this.intValue = intValue;
    }

    ApplicationEnum(long longValue){
        this.longValue = longValue;
    }


    private String stringValue;
    public String getStringValue() {
        return stringValue;
    }

    private int intValue;
    public int getIntValue() {
        return intValue;
    }

    private long longValue;
    public long getLongValue() {
        return longValue;
    }

}
