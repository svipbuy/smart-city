package com.sc.common.util;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.cache.CacheFactory;
import com.sc.common.exception.BusinessException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.core.env.PropertiesPropertySource;

/**
 * mq发送工具
 */
public class MqSenderUtil {
    private MqSenderUtil(){}


    /**
     * 此处是通知各个缓存业务对象去更新缓存，复杂的缓存需要通过这种形式去维护数据库与缓存数据的同步
     * @param source
     * @param data
     * @param opt
     */
    public static void cacheSender(String source, Object data, String opt){
        PropertiesPropertySource propertiesPropertySource = CommonUtil.getPropertiesPropertySource("rabbitmqProperties");
        if(propertiesPropertySource == null){
            return;
        }

        PropertiesPropertySource propertiesPropertySource4applicationProperties = CommonUtil.getPropertiesPropertySource("applicationProperties");
        if(propertiesPropertySource4applicationProperties == null){
            return;
        }

        String cacheExchange = propertiesPropertySource.getProperty("spring.rabbitmq.cache.exchange.name").toString();
        String cacheRoutingKey = propertiesPropertySource.getProperty("spring.rabbitmq.cache.routing-key").toString();
        String applicationName = propertiesPropertySource4applicationProperties.getProperty("spring.application.name").toString();


        if(MyStringUtils.isBlank(MyStringUtils.null2String(applicationName))){
           throw new BusinessException("未知的应用服务名[" + applicationName + "]");
        }


        CacheFactory cacheFactory = SpringContextHolder.getBean("cacheFactory");
        if(cacheFactory.getCacheBo(source) == null){
            // 缓存工厂没有配置该持久化对象的缓存，忽略
            return;
        }

        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("spring.application.name",applicationName);
        jsonObject.put("category",source);
        jsonObject.put("opt", opt);
        jsonObject.put("data",data);

        RabbitTemplate rabbitTemplate = SpringContextHolder.getBean("rabbitTemplate");
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.convertAndSend(cacheExchange,cacheRoutingKey,jsonObject);
    }
}
