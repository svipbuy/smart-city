package com.sc.common.interceptors.dataprivilege;


import com.sc.common.util.ReflectHelper;
import org.apache.ibatis.executor.statement.BaseStatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.springframework.stereotype.Service;

/**
 * @author ：wust
 * @date ：Created in 2019/8/8 10:04
 * @description：平台管理员账号类型策略，能看到所有数据
 * @version:
 */
@Service("adminAccountStrategy")
public class AdminAccountStrategy implements IStrategy {
    @Override
    public void bindSql(BaseStatementHandler delegate) {

        BoundSql boundSql = delegate.getBoundSql();

        String sql = boundSql.getSql();

        StringBuffer privilegeSqlStringBuffer = new StringBuffer("SELECT privilege_tmp.* FROM (" + sql + ") privilege_tmp");

        ReflectHelper.setValueByFieldName(boundSql, "sql", privilegeSqlStringBuffer.toString());
    }
}
