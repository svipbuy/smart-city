/**
 * Created by wust on 2019-10-31 15:52:48
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.common.util;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.mail.SimpleEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URLDecoder;

/**
 * @author: wust
 * @date: Created in 2019-10-31 15:52:48
 * @description: 生成验证码工具
 *
 */
public class GenVerificationCodeUtil {
    static Logger logger = LogManager.getLogger(GenVerificationCodeUtil.class);

    public static String sendMsg(String url, String account, String pwd, String mobile, String msg) throws Exception {
        HttpClient client = new HttpClient();
        GetMethod method = new GetMethod();
        try {
            URI base = new URI(url, false);
            method.setURI(new URI(base, "HttpBatchSendSM", false));
            method.setQueryString(new NameValuePair[] {
                    new NameValuePair("account", account),
                    new NameValuePair("pswd", pwd),
                    new NameValuePair("mobile", mobile),
                    new NameValuePair("needstatus", String.valueOf(false)),
                    new NameValuePair("msg", msg),
                    new NameValuePair("extno", null), });
            int result = client.executeMethod(method);
            logger.info("Http sendMsg result:    "+result);
            if (result == HttpStatus.SC_OK) {
                InputStream in = method.getResponseBodyAsStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int len = 0;
                while ((len = in.read(buffer)) != -1) {
                    baos.write(buffer, 0, len);
                }
                logger.info("Http sendMsg baos:    "+ URLDecoder.decode(baos.toString(), "UTF-8"));
                return URLDecoder.decode(baos.toString(), "UTF-8");
            } else {
                logger.error("HTTP ERROR Status: " + method.getStatusCode() + ":" + method.getStatusText());
                throw new Exception("HTTP ERROR Status: " + method.getStatusCode() + ":" + method.getStatusText());
            }
        } finally {
            method.releaseConnection();
        }
    }


    /**
     * 发送邮件
     * @param hostName
     * @param fromUserName
     * @param fromUserPwd
     * @param toUserName
     * @param subject
     * @param content
     * @return
     */
    public static boolean sendMail(String hostName, String fromUserName, String fromUserPwd, String toUserName, String subject, String content) {
        boolean result = true;
        SimpleEmail email = new SimpleEmail();
        email.setHostName(hostName);
        email.setAuthentication(fromUserName, fromUserPwd);// 你的邮箱帐号和密码
        email.setSSLOnConnect(true);
        email.setSslSmtpPort("465");  //使用465端口(不设置也可，ssl默认为465)
        try {
            email.setFrom(fromUserName); // 发送方
            email.addTo(toUserName); // 接收方
            email.setCharset("UTF8"); // 字符编码方式
            email.setSubject(subject); // 邮件标题
            email.setMsg(content); // 内容
            email.send();
        } catch (Exception e) {
            result = false;
            logger.error("发送邮件失败",e);
        }
        return result;
    }
}
