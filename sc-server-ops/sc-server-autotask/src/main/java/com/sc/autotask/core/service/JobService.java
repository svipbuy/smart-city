package com.sc.autotask.core.service;

import com.sc.autotask.entity.jobandtrigger.QrtzJobAndTriggerList;
import com.sc.autotask.entity.jobandtrigger.QrtzJobAndTriggerSearch;
import java.util.List;

/**
 * Created by wust on 2019/6/13.
 */
public interface JobService {
    List<QrtzJobAndTriggerList> selectByPageNumSize(
            QrtzJobAndTriggerSearch search,
            int pageNum,
            int pageSize
    );
}
