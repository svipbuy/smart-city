/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.bo.DataSourceBo;
import com.sc.admin.core.dao.SysDataSourceMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.datasource.SysDataSource;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 数据源缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysDataSource")
@Component
public class RedisCacheDataSourceBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysDataSourceMapper sysDataSourceMapper;

    @Autowired
    private DataSourceBo dataSourceBo;

    @Override
    public void init() {
        Set<String> keys2 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME_AND_PROJECT_ID.getStringValue().replaceAll("%s_","*"));
        if(keys2 != null && keys2.size() > 0){
            springRedisTools.deleteByKey(keys2);
        }

        Set<String> keys3 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME.getStringValue().replaceAll("%s_","*"));
        if(keys3 != null && keys3.size() > 0){
            springRedisTools.deleteByKey(keys3);
        }

        cacheByProjectId();
        cacheByGroupName();
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        SysDataSource entity = null;

        if(obj instanceof Long){
            entity = sysDataSourceMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof SysDataSource){
            entity = (SysDataSource)obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),SysDataSource.class);
        }

        if(entity == null){
            return;
        }

        cacheByProjectId(entity.getGroupName(),entity.getProjectId());
        cacheByGroupName(entity.getGroupName());
    }

    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){
        if(primaryKey == null){
            return;
        }

        SysDataSource entity = sysDataSourceMapper.selectByPrimaryKey(primaryKey);
        if(entity != null){
            cacheByProjectId(entity.getGroupName(),entity.getProjectId());
            cacheByGroupName(entity.getGroupName());
        }
    }


    @Override
    public void batchUpdate(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey){
        SysDataSource dataSource = sysDataSourceMapper.selectByPrimaryKey(primaryKey);
        if(dataSource != null){
            String key3 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME_AND_PROJECT_ID.getStringValue(),dataSource.getGroupName(),dataSource.getProjectId());
            if(springRedisTools.hasKey(key3)){
                springRedisTools.deleteByKey(key3);
            }

            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME.getStringValue(),dataSource.getGroupName());
            if(springRedisTools.hasKey(key2)){
                springRedisTools.deleteByKey(key2);
            }
        }
    }

    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }



    private void cacheByProjectId(String groupName,Long projectId){
        Map<String,Map<Long,SysDataSource>> map1 = dataSourceBo.groupByProjectId(groupName,projectId);
        if(map1 != null && map1.size() > 0){
            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME_AND_PROJECT_ID.getStringValue(),groupName,projectId);
            if(springRedisTools.hasKey(key2)){
                springRedisTools.deleteByKey(key2);
            }
            springRedisTools.addMap(key2,map1);
        }
    }


    private void cacheByProjectId(){
        Map<String,Map<Long,SysDataSource>> map1 = dataSourceBo.groupByProjectId();
        if(map1 != null && map1.size() > 0){
            Set<Map.Entry<String,Map<Long,SysDataSource>>> entrySet = map1.entrySet();
            for (Map.Entry<String, Map<Long,SysDataSource>> stringListEntry : entrySet) {
                String key = stringListEntry.getKey();
                Map<Long,SysDataSource> value = stringListEntry.getValue();
                String groupName = key.split("\\.")[0];
                String projectId = key.split("\\.")[1];
                String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME_AND_PROJECT_ID.getStringValue(),groupName,projectId);
                if(springRedisTools.hasKey(key2)){
                    springRedisTools.deleteByKey(key2);
                }
                springRedisTools.addMap(key2,value);
            }
        }
    }


    private void cacheByGroupName(String groupName){
        Map<String,Map<Long,SysDataSource>> map1 = dataSourceBo.groupByGroupName(groupName);
        if(map1 != null && map1.size() > 0){
            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME.getStringValue(),groupName);
            if(springRedisTools.hasKey(key2)){
                springRedisTools.deleteByKey(key2);
            }
            springRedisTools.addMap(key2,map1.get(groupName));
        }
    }


    private void cacheByGroupName(){
        Map<String,Map<Long,SysDataSource>> map1 = dataSourceBo.groupByGroupName();
        if(map1 != null && map1.size() > 0){
            Set<Map.Entry<String,Map<Long,SysDataSource>>> entrySet = map1.entrySet();
            for (Map.Entry<String, Map<Long,SysDataSource>> stringListEntry : entrySet) {
                String key = stringListEntry.getKey();
                String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_DATA_SOURCE_BY_GROUP_NAME.getStringValue(),key);
                if(springRedisTools.hasKey(key2)){
                    springRedisTools.deleteByKey(key2);
                }
                springRedisTools.addMap(key2,stringListEntry.getValue());
            }
        }
    }
}
