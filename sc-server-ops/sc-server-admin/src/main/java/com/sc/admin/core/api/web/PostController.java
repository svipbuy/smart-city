package com.sc.admin.core.api.web;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.common.dto.PageDto;
import com.sc.admin.entity.post.SysPost;
import com.sc.admin.entity.post.SysPostList;
import com.sc.admin.entity.post.SysPostSearch;
import com.sc.admin.core.service.SysPostService;
import com.sc.common.annotations.WebApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.ArrayList;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.context.DefaultBusinessContext;
import java.util.Date;
import cn.hutool.core.collection.CollectionUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.core.env.Environment;

/**
 * @author: wust
 * @date: 2020-12-03 11:14:47
 * @description:
 */
@WebApi
@RequestMapping("/web/v1/PostController")
@RestController
public class PostController {
    @Autowired
    private SysPostService sysPostServiceImpl;

    @Autowired
    private Environment environment;


   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody SysPostSearch search){
      WebResponseDto responseDto = new WebResponseDto();

      DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

      PageDto pageDto = search.getPageDto();
      Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());
      List<SysPost> list = sysPostServiceImpl.select(search);
      if (CollectionUtil.isNotEmpty(list)) {
          List<SysPostList> lists = new ArrayList<>(list.size());
          for (SysPost entity : list) {
              SysPostList entityList = new SysPostList();
              BeanUtils.copyProperties(entity,entityList);
              lists.add(entityList);
          }
          responseDto.setLstDto(lists);
      }

      BeanUtils.copyProperties(page,pageDto);
      responseDto.setPage(pageDto);
      return responseDto;
   }



   @RequestMapping(value = "",method = RequestMethod.POST)
   public WebResponseDto create(@RequestBody  SysPost entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setCreaterId(ctx.getAccountId());
       entity.setCreaterName(ctx.getAccountName());
       entity.setCreateTime(new Date());
       sysPostServiceImpl.insert(entity);
       return responseDto;
   }


   @RequestMapping(value = "",method = RequestMethod.PUT)
   public WebResponseDto update(@RequestBody  SysPost entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
       entity.setModifyId(ctx.getAccountId());
       entity.setModifyName(ctx.getAccountName());
       entity.setModifyTime(new Date());
       sysPostServiceImpl.updateByPrimaryKeySelective(entity);
       return responseDto;
   }


   @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
   public WebResponseDto delete(@PathVariable Long id){
       WebResponseDto responseDto = new WebResponseDto();
       sysPostServiceImpl.deleteByPrimaryKey(id);
       return responseDto;
   }



    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public WebResponseDto detail(@PathVariable Long id){
        WebResponseDto responseDto = new WebResponseDto();
        SysPost entity = sysPostServiceImpl.selectByPrimaryKey(id);
        if(entity != null){
            responseDto.setObj(entity);
        }else{
            responseDto.setFlag(WebResponseDto.INFO_WARNING);
            responseDto.setMessage("没有该记录！");
            return responseDto;
        }
        return responseDto;
    }
}
