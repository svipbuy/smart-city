/**
 * Created by wust on 2020-04-18 10:31:04
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.bo;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.admin.core.dao.SysUserOrganizationMapper;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.entity.admin.userorganization.SysUserOrganization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * @author: wust
 * @date: Created in 2020-04-18 10:31:04
 * @description:
 *
 */
@Component
public class UserOrganizationBo {
    @Autowired
    private SysUserOrganizationMapper sysUserOrganizationMapper;

    public Map<Long, Set<Long>> groupByUserId(){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        final Map<Long, Set<Long>> map = new HashMap<>();
        SysUserOrganization userOrganizationSearch = new SysUserOrganization();
        userOrganizationSearch.setUserId(ctx.getAccountId());
        userOrganizationSearch.setAgentId(ctx.getAgentId());
        List<SysUserOrganization> userOrganizations =  sysUserOrganizationMapper.select(userOrganizationSearch);
        if(CollectionUtil.isNotEmpty(userOrganizations)){
            for (SysUserOrganization userOrganization : userOrganizations) {
                Long key = userOrganization.getUserId();
                if(map.containsKey(key)){
                    Set<Long> set =  map.get(key);
                    set.add(userOrganization.getAgentId());
                    set.add(userOrganization.getParentCompanyId());
                    set.add(userOrganization.getBranchCompanyId());
                    set.add(userOrganization.getProjectId());
                    set.add(userOrganization.getDepartmentId());
                    set.add(userOrganization.getRoleId());
                    map.put(key,set);
                }else{
                    Set<Long> set =  new HashSet<>();
                    set.add(userOrganization.getAgentId());
                    set.add(userOrganization.getParentCompanyId());
                    set.add(userOrganization.getBranchCompanyId());
                    set.add(userOrganization.getProjectId());
                    set.add(userOrganization.getDepartmentId());
                    set.add(userOrganization.getRoleId());
                    map.put(key,set);
                }
            }
        }
        return map;
    }
}
