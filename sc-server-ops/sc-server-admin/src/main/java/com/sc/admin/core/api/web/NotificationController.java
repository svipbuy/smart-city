/**
 * Created by wust on 2019-10-21 14:38:24
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.web;

import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sc.admin.core.bo.NotificationBo;
import com.sc.admin.core.service.*;
import com.sc.admin.core.service.*;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.notification.SysNotification;
import com.sc.common.entity.admin.notification.SysNotificationList;
import com.sc.common.entity.admin.notification.SysNotificationSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.MyStringUtils;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: wust
 * @date: Created in 2019-10-21 14:38:24
 * @description: 发布通知
 *
 */
@WebApi
@RequestMapping("/web/v1/NotificationController")
@RestController
public class NotificationController {

    @Autowired
    private SysNotificationService sysNotificationServiceImpl;

    @Autowired
    private SysCompanyService sysCompanyServiceImpl;

    @Autowired
    private SysProjectService sysProjectServiceImpl;

    @Autowired
    private SysDepartmentService sysDepartmentServiceImpl;

    @Autowired
    private SysRoleService sysRoleServiceImpl;

    @Autowired
    private NotificationBo notificationBo;

    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_NOTIFICATION,businessName="分页查询",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/listPage",method = RequestMethod.POST)
    public WebResponseDto listPage(@RequestBody SysNotificationSearch search){
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        WebResponseDto responseDto = new WebResponseDto();

        PageDto pageDto = search.getPageDto();
        Page page = PageHelper.startPage(pageDto.getPageNum(),pageDto.getPageSize());

        List<SysNotification> notifications =  sysNotificationServiceImpl.select(search);
        if(CollectionUtils.isNotEmpty(notifications)){
            List<SysNotificationList> notificationLists = new ArrayList<>(notifications.size());
            for (SysNotification notification : notifications) {
                SysNotificationList notificationList = new SysNotificationList();
                BeanUtils.copyProperties(notification,notificationList);
                notificationList.setStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),notification.getStatus()));
                notificationList.setReceiverTypeLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),notification.getReceiverType()));
                notificationList.setPriorityLevelLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),notification.getPriorityLevel()));
                notificationList.setTypeLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),notification.getType()));

                if(MyStringUtils.isNotBlank(MyStringUtils.null2String(notification.getSendChannel()))){
                    String sendChannelLabel = "";
                    String[] sendChannels = notification.getSendChannel().split(",");
                    for (String sendChannel : sendChannels) {
                        if(MyStringUtils.isBlank(MyStringUtils.null2String(sendChannel))){
                            continue;
                        }
                        if(MyStringUtils.isBlank(sendChannelLabel)){
                            sendChannelLabel += DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(), sendChannel);
                        }else{
                            sendChannelLabel += "," + DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(), sendChannel);
                        }
                    }
                    notificationList.setSendChannelLabel(sendChannelLabel);
                }
                notificationLists.add(notificationList);
            }
            responseDto.setLstDto(notificationLists);
        }

        BeanUtils.copyProperties(page,pageDto);
        responseDto.setPage(pageDto);
        return responseDto;
    }

    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_NOTIFICATION,businessName="新建",operationType= OperationLogEnum.Insert)
    @RequestMapping(value = "",method = RequestMethod.POST)
    public WebResponseDto create(@RequestBody SysNotification entity){
        WebResponseDto mm = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        entity.setStatus("A101301");
        entity.setSendChannel(entity.getSendChannel()); // 默认所有渠道都发送
        entity.setType("A102107"); // 系统通知
        entity.setCreaterId(ctx.getAccountId());
        entity.setCreaterName(ctx.getAccountName());
        entity.setCreateTime(new Date());
        sysNotificationServiceImpl.insert(entity);
        return mm;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_NOTIFICATION,businessName="修改",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "",method = RequestMethod.PUT)
    public WebResponseDto update(@RequestBody SysNotification entity){
        WebResponseDto mm = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();
        entity.setModifyId(ctx.getAccountId());
        entity.setModifyName(ctx.getAccountName());
        entity.setModifyTime(new Date());
        sysNotificationServiceImpl.updateByPrimaryKey(entity);
        return mm;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_NOTIFICATION,businessName="删除",operationType= OperationLogEnum.Delete)
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public WebResponseDto delete(@PathVariable Long id){
        WebResponseDto mm = new WebResponseDto();
        sysNotificationServiceImpl.deleteByPrimaryKey(id);
        return mm;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_NOTIFICATION,businessName="发布",operationType= OperationLogEnum.Update)
    @RequestMapping(value = "/publish/{id}",method = RequestMethod.PUT)
    public WebResponseDto publish(@PathVariable Long id){
        WebResponseDto responseDto = sysNotificationServiceImpl.publish(id);
        return responseDto;
    }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_NOTIFICATION,businessName="获取接收者级联数据",operationType= OperationLogEnum.Search)
    @RequestMapping(value = "/buildReceiverCascader/{receiverType}",method = RequestMethod.GET)
    public WebResponseDto buildReceiverCascader(@PathVariable String receiverType){
        WebResponseDto responseDto = new WebResponseDto();
        DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

        JSONArray jsonArray = notificationBo.buildReceiverCascaderByReceiverType(receiverType);
        responseDto.setObj(jsonArray.toJSONString());
        return responseDto;
    }
}
