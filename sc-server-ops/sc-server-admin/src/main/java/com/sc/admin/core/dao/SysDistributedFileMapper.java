package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;

public interface SysDistributedFileMapper extends IBaseMapper<SysDistributedFile> {
}
