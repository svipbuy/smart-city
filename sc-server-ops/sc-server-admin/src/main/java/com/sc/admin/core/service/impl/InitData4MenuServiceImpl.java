/**
 * Created by wust on 2020-03-31 09:13:01
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.admin.core.dao.SysLookupMapper;
import com.sc.admin.core.dao.SysMenuMapper;
import com.sc.admin.core.dao.SysResourceMapper;
import com.sc.admin.core.dao.SysRoleResourceMapper;
import com.sc.admin.core.xml.XMLAbstractResolver;
import com.sc.admin.core.xml.XMLDefinitionFactory;
import com.sc.admin.core.xml.factory.XMLPermissionFactory;
import com.sc.common.entity.admin.menu.SysMenu;
import com.sc.common.entity.admin.resource.SysResource;
import com.sc.common.entity.admin.role.resource.SysRoleResource;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.service.InitializtionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author: wust
 * @date: Created in 2020-03-31 09:13:01
 * @description:
 *
 */
@Order(1)
@Service
public class InitData4MenuServiceImpl implements InitializtionService {
    @Autowired
    private SysLookupMapper sysLookupMapper;

    @Autowired
    private SysMenuMapper sysMenuMapper;

    @Autowired
    private SysResourceMapper sysResourceMapper;

    @Autowired
    private SysRoleResourceMapper sysRoleResourceMapper;

    @Transactional(rollbackFor=Exception.class)
    @Override
    public void init() {
        XMLDefinitionFactory xmlPermissionFactory = new XMLPermissionFactory();
        XMLAbstractResolver resolver = xmlPermissionFactory.createXMLResolver();
        Map<String,List> resultMap =  resolver.getResult();

        sysMenuMapper.deleteAll();

        sysResourceMapper.deleteAll();

        List<SysMenu> parseMenuList = resultMap.get("parseMenuList") == null ? null : (List<SysMenu>) resultMap.get("parseMenuList");
        List<SysResource> parseResourceList = resultMap.get("parseResourceList") == null ? null : (List<SysResource>)resultMap.get("parseResourceList");


        if(!org.springframework.util.CollectionUtils.isEmpty(parseMenuList)){
            sysMenuMapper.insertList(parseMenuList);
        }


        if(!org.springframework.util.CollectionUtils.isEmpty(parseResourceList)){
            sysResourceMapper.insertList(parseResourceList);
        }



        /**
         * 清空已经分配给角色的脏菜单（因为xml可能删除了一些菜单）
         */
        sysRoleResourceMapper.deleteDirtyMenu();



        /**
         * 清空已经分配给角色的脏资源（因为xml可能删除了一些资源）
         */
        sysRoleResourceMapper.deleteDirtyResource();




        /**
         * 重新为所有角色分配拥有菜单的白名单
         */
        SysRoleResource roleResourceSearch = new SysRoleResource();
        roleResourceSearch.setType(ApplicationEnum.MENU_TYPE_M.getStringValue());
        List<SysRoleResource> roleResources = sysRoleResourceMapper.select(roleResourceSearch);
        if(!org.springframework.util.CollectionUtils.isEmpty(roleResources)){
            for (SysRoleResource roleResource : roleResources) {
                Long roleId = roleResource.getRoleId();
                String menuCode = roleResource.getResourceCode();
                Long organizationId = roleResource.getOrganizationId();

                /**
                 * 找到该菜单下面的白名单资源
                 */
                List<SysRoleResource> anonList = new ArrayList<>(20);
                List<SysResource> sysResources4anon = sysResourceMapper.findAnonResourcesByMenuId(menuCode);
                if(!CollectionUtils.isEmpty(sysResources4anon)){
                    for (SysResource sysResource : sysResources4anon) {
                        SysRoleResource anon = new SysRoleResource();
                        anon.setOrganizationId(organizationId);
                        anon.setResourceCode(sysResource.getCode());
                        anon.setRoleId(roleId);
                        anon.setType(ApplicationEnum.MENU_TYPE_R.getStringValue());
                        List<SysRoleResource> anons = sysRoleResourceMapper.select(anon);
                        if(CollectionUtil.isEmpty(anons)){
                            anon.setCreateTime(new Date());
                            anon.setIsDeleted(0);
                            anonList.add(anon);
                        }
                    }
                    if(CollectionUtil.isNotEmpty(anonList)){
                        sysRoleResourceMapper.insertList(anonList);
                    }
                }
            }
        }
    }
}
