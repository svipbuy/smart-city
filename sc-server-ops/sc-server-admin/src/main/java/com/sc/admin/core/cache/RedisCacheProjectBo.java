/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.sc.admin.core.dao.SysProjectMapper;
import com.sc.common.annotations.EnableComplexCaching;
import com.sc.common.entity.admin.project.SysProject;
import com.sc.common.enums.RedisKeyEnum;
import com.sc.common.cache.CacheAbstract;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 项目缓存
 *
 */
@EnableComplexCaching(dependsOnPOSimpleName = "SysProject")
@Component
public class RedisCacheProjectBo extends CacheAbstract {
    @Autowired
    private SpringRedisTools springRedisTools;

    @Autowired
    private SysProjectMapper sysProjectMapper;

    @Override
    public void init() {
        Set<String> keys1 = springRedisTools.keys(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_NAME.getStringValue().replaceAll("%s_","*"));
        if(keys1 != null && keys1.size() > 0){
            springRedisTools.deleteByKey(keys1);
        }

        List<SysProject> projectList =  sysProjectMapper.selectAll();
        if(CollectionUtil.isNotEmpty(projectList)){
            for (SysProject project : projectList) {
                cacheByName(project);
            }
        }
    }

    @Override
    public void reset() {

    }

    @Override
    public void add(Object obj){
        if(obj == null){
            return;
        }

        SysProject entity = null;

        if(obj instanceof Long){
            entity = sysProjectMapper.selectByPrimaryKey(obj);
        }else if(obj instanceof SysProject){
            entity = (SysProject)obj;
        }else if(obj instanceof Map){
            entity = JSONObject.parseObject(JSONObject.toJSONString(obj),SysProject.class);
        }

        if(entity == null){
            return;
        }

        cacheByName(entity);
    }

    @Override
    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }

    @Override
    public void updateByPrimaryKey(Object primaryKey){
        SysProject project =  sysProjectMapper.selectByPrimaryKey(primaryKey);
        if(project != null){
            cacheByName(project);
        }
    }

    @Override
    public void batchUpdate(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                updateByPrimaryKey(o);
            }
        }
    }

    @Override
    public void deleteByPrimaryKey(Object primaryKey){
        SysProject project =  sysProjectMapper.selectByPrimaryKey(primaryKey);
        if(project != null){
            String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_NAME.getStringValue(),project.getName());
            if(springRedisTools.hasKey(key2)){
                springRedisTools.deleteByKey(key2);
            }
        }
    }

    @Override
    public void batchDelete(List<Object> primaryKeys){
        if(CollectionUtil.isNotEmpty(primaryKeys)){
            for (Object primaryKey : primaryKeys) {
                deleteByPrimaryKey(primaryKey);
            }
        }
    }

    private void cacheByName(SysProject project){
        if(project == null){
            return;
        }

        String key2 = String.format(RedisKeyEnum.REDIS_KEY_HASH_GROUP_PROJECT_BY_NAME.getStringValue(),project.getName());
        Map mapValue2 = BeanUtil.beanToMap(project);
        springRedisTools.addMap(key2,mapValue2);
    }
}
