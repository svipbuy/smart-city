package com.sc.admin.core.api.web;


import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.sc.admin.core.bo.CustomerBo;
import com.sc.admin.core.service.SysAccountService;
import com.sc.admin.core.service.SysCustomerService;
import com.sc.common.annotations.OperationLog;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.dto.PageDto;
import com.sc.common.entity.admin.account.SysAccount;
import com.sc.common.entity.admin.customer.SysCustomer;
import com.sc.common.entity.admin.customer.SysCustomerList;
import com.sc.common.entity.admin.customer.SysCustomerSearch;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.util.MyIdUtil;
import com.sc.common.util.cache.DataDictionaryUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2019-12-27 11:37:51
 * @description:
 */
@WebApi
@RequestMapping("/web/v1/CustomerController")
@RestController
public class CustomerController {
    @Autowired
    private SysCustomerService sysCustomerServiceImpl;

    @Autowired
    private CustomerBo customerBo;

    @Autowired
    private SysAccountService sysAccountServiceImpl;

   @RequestMapping(value = "/listPage",method = RequestMethod.POST)
   public WebResponseDto listPage(@RequestBody SysCustomerSearch search){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       if(ctx.isStaff()){ // 非管理员只能查看指定范围的数据
           search.setAgentId(ctx.getAgentId());
           search.setParentCompanyId(ctx.getParentCompanyId());
           search.setBranchCompanyId(ctx.getBranchCompanyId());
           search.setProjectId(ctx.getProjectId());
       }

       PageDto pageDto = search.getPageDto();
       List<SysCustomerList> list =  sysCustomerServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize()) == null ? null
               : (List<SysCustomerList>)sysCustomerServiceImpl.selectByPageNumSize(search,pageDto.getPageNum(),pageDto.getPageSize());
       if(CollectionUtil.isNotEmpty(list)){
           List<SysCustomerList> customerLists = new ArrayList<>(list.size());
           for (SysCustomerList customerList : list) {
               customerList.setTypeLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),customerList.getType()));
               customerList.setSexLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),customerList.getSex()));
               customerList.setStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),customerList.getStatus()));
               customerList.setApplyStatusLabel(DataDictionaryUtil.getLookupNameByCode(ctx.getLocale().toString(),customerList.getApplyStatus()));
               customerLists.add(customerList);
           }
       }
       PageInfo page = new PageInfo(list);
       BeanUtils.copyProperties(page,pageDto);
       responseDto.setPage(pageDto);
       responseDto.setLstDto(list);
       return responseDto;
   }



   @RequestMapping(value = "",method = RequestMethod.POST)
   public WebResponseDto create(@RequestBody SysCustomer entity){
       WebResponseDto responseDto = new WebResponseDto();
       DefaultBusinessContext ctx = DefaultBusinessContext.getContext();

       SysCustomer customerSearch = new SysCustomer();
       customerSearch.setLoginName(entity.getLoginName());
       SysCustomer customerExist = sysCustomerServiceImpl.selectOne(customerSearch);
       if(customerExist != null){
           responseDto.setFlag(WebResponseDto.INFO_WARNING);
           responseDto.setMessage("该账号与系统现有账号有重复，请仔细核对");
           return responseDto;
       }

       SysAccount accountSearch = new SysAccount();
       accountSearch.setAccountCode(entity.getLoginName());
       SysAccount account = sysAccountServiceImpl.selectOne(accountSearch);
       if (account == null) {
           account = new SysAccount();
           account.setId(MyIdUtil.getId());
           account.setAccountCode(entity.getLoginName());
           account.setAccountName(entity.getName());
           account.setType("A101705");
           account.setSource("Web");
           account.setCreaterId(ctx.getAccountId());
           account.setCreaterName(ctx.getAccountName());
           account.setCreateTime(new Date());
       }

       entity.setId(account.getId());
       entity.setAgentId(ctx.getAgentId());
       entity.setStatus("A100201");
       entity.setCreaterId(ctx.getAccountId());
       entity.setCreaterName(ctx.getAccountName());
       entity.setCreateTime(new Date());

       JSONObject jsonObject = new JSONObject();
       jsonObject.put("account",account);
       jsonObject.put("customer",entity);
       sysCustomerServiceImpl.create(jsonObject);
       return responseDto;
   }


   @RequestMapping(value = "",method = RequestMethod.PUT)
   public WebResponseDto update(@RequestBody  SysCustomer entity){
       WebResponseDto responseDto = new WebResponseDto();
       sysCustomerServiceImpl.update(entity);
       return responseDto;
   }


    @OperationLog(moduleName= OperationLogEnum.MODULE_ADMIN_CUSTOMER,businessName="删除",operationType= OperationLogEnum.Delete)
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public WebResponseDto delete(@PathVariable Long id){
        WebResponseDto responseDto = new WebResponseDto();
        responseDto = sysCustomerServiceImpl.delete(id);
        return responseDto;
    }

    @RequestMapping(value = "/buildCascader",method = RequestMethod.POST)
    public WebResponseDto buildCascader(){
        WebResponseDto responseDto = new WebResponseDto();
        JSONArray jsonArray = customerBo.buildCascader();
        responseDto.setObj(jsonArray);
        return responseDto;
    }
}
