package com.sc.admin.core.service;

import com.sc.common.service.BaseService;
import com.sc.admin.entity.tenant.SysTenant;

/**
 * @author: wust
 * @date: 2020-12-03 10:00:18
 * @description:
 */
public interface SysTenantService extends BaseService<SysTenant>{
}
