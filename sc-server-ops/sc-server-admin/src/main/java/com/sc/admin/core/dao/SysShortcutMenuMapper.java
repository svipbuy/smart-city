package com.sc.admin.core.dao;

import com.sc.common.mapper.IBaseMapper;
import com.sc.common.entity.admin.shortcutmenu.SysShortcutMenu;

/**
 * @author: wust
 * @date: 2020-02-16 11:25:19
 * @description:
 */
public interface SysShortcutMenuMapper extends IBaseMapper<SysShortcutMenu>{
}