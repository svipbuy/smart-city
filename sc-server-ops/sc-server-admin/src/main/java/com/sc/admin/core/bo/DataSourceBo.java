package com.sc.admin.core.bo;

import cn.hutool.core.collection.CollectionUtil;
import com.sc.admin.core.dao.SysDataSourceMapper;
import com.sc.common.entity.admin.datasource.SysDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class DataSourceBo {
    @Autowired
    private SysDataSourceMapper sysDataSourceMapper;

    public Map<String, Map<Long,SysDataSource>> groupByGroupName(){
        Map<String, Map<Long,SysDataSource>> map1 = new HashMap<>();
        List<SysDataSource> dataSources = sysDataSourceMapper.selectAll();
        if(CollectionUtil.isNotEmpty(dataSources)){
            for (SysDataSource dataSource : dataSources) {
                String key = dataSource.getGroupName();
                if(map1.containsKey(key)){
                    Map<Long,SysDataSource> dataSourceMap = map1.get(key);
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }else{
                    Map<Long,SysDataSource> dataSourceMap = new HashMap<>();
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }
            }
        }
        return map1;
    }

    public Map<String, Map<Long,SysDataSource>> groupByGroupName(String groupName){
        Map<String, Map<Long,SysDataSource>> map1 = new HashMap<>();
        SysDataSource dataSourceSearch = new SysDataSource();
        dataSourceSearch.setGroupName(groupName);
        List<SysDataSource> dataSources = sysDataSourceMapper.select(dataSourceSearch);
        if(CollectionUtil.isNotEmpty(dataSources)){
            for (SysDataSource dataSource : dataSources) {
                String key = dataSource.getGroupName();
                if(map1.containsKey(key)){
                    Map<Long,SysDataSource> dataSourceMap = map1.get(key);
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }else{
                    Map<Long,SysDataSource> dataSourceMap = new HashMap<>();
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }
            }
        }
        return map1;
    }



    public Map<String, Map<Long,SysDataSource>> groupByProjectId(){
        Map<String, Map<Long,SysDataSource>> map1 = new HashMap<>();
        List<SysDataSource> dataSources = sysDataSourceMapper.selectAll();
        if(CollectionUtil.isNotEmpty(dataSources)){
            for (SysDataSource dataSource : dataSources) {
                String key = dataSource.getGroupName() + "." + dataSource.getProjectId();
                if(map1.containsKey(key)){
                    Map<Long,SysDataSource> dataSourceMap = map1.get(key);
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }else{
                    Map<Long,SysDataSource> dataSourceMap = new HashMap<>();
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }
            }
        }
        return map1;
    }

    public Map<String, Map<Long,SysDataSource>> groupByProjectId(String groupName,Long projectId){
        Map<String, Map<Long,SysDataSource>> map1 = new HashMap<>();
        SysDataSource dataSourceSearch = new SysDataSource();
        dataSourceSearch.setProjectId(projectId);
        dataSourceSearch.setGroupName(groupName);
        List<SysDataSource> dataSources = sysDataSourceMapper.select(dataSourceSearch);
        if(CollectionUtil.isNotEmpty(dataSources)){
            for (SysDataSource dataSource : dataSources) {
                String key = dataSource.getGroupName() + "." + dataSource.getProjectId();
                if(map1.containsKey(key)){
                    Map<Long,SysDataSource> dataSourceMap = map1.get(key);
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }else{
                    Map<Long,SysDataSource> dataSourceMap = new HashMap<>();
                    dataSourceMap.put(dataSource.getId(),dataSource);
                    map1.put(key,dataSourceMap);
                }
            }
        }
        return map1;
    }
}
