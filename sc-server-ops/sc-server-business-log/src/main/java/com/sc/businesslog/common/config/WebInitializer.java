package com.sc.businesslog.common.config;


import com.sc.common.BaseWebInitializer;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.enums.ApplicationEnum;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 系统启动，初始化相关数据的入口
 * Created by wust on 2019/6/12.
 */
@Component
public class WebInitializer extends BaseWebInitializer implements ApplicationListener<ApplicationReadyEvent> {

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        DefaultBusinessContext.getContext().setDataSourceId(ApplicationEnum.DEFAULT.name());

        ConfigurableApplicationContext configurableApplicationContext = applicationReadyEvent.getApplicationContext();

        intHandle(configurableApplicationContext);
    }

    @Override
    protected String getEntityPath() {
        return "";
    }

    @Override
    protected String[] getCacheNames() {
        return null;
    }
}
