package com.sc.businesslog.core.dao;

import com.sc.businesslog.entity.BlOperationLog;
import com.sc.common.mapper.IBaseMapper;

/**
 * Created by wust on 2019/5/28.
 */
public interface BlOperationLogMapper extends IBaseMapper<BlOperationLog> {

}
