package com.sc.admin.entity.tenant;

import com.sc.common.entity.BaseEntity;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 10:00:18
 * @description:
 */
@ApiModel(description = "实体对象-SysTenant")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class SysTenant extends BaseEntity {
    private String code;
    private String nameFullName;
    private String shortName;
    private String contactPerson;
    private String contactPersonMobilePhone;
    private String registeredAddress;
    private String currentAddress;
    private String fixedLineTelephone;
    private String email;
    private String state;
    private String description;

}