package com.sc.admin.entity.tenant;


import com.sc.common.dto.PageDto;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: wust
 * @date: 2020-12-03 10:21:44
 * @description:
 */
@ApiModel(description = "查询对象-SysTenantAuthenticationSearch")
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Data
public class SysTenantAuthenticationSearch extends SysTenantAuthentication {
    private PageDto pageDto;
}